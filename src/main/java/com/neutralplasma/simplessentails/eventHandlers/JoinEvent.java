package com.neutralplasma.simplessentails.eventHandlers;

import com.neutralplasma.simplessentails.SimpleEssentials;
import com.neutralplasma.simplessentails.flyManager.FlyTimer;
import com.neutralplasma.simplessentails.utils.PlayerUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;


public class JoinEvent implements Listener {
    private PlayerUtil playerUtil;

    public JoinEvent(PlayerUtil playerUtil){
        this.playerUtil = playerUtil;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();
        String ipAddress = player.getAddress().getAddress().toString();
        FlyTimer.getInstance().addPlayer(player.getUniqueId());
        playerUtil.updatePlayerDataString(player,"ipAddress", ipAddress);
    }
}
