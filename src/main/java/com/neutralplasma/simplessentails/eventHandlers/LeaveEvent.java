package com.neutralplasma.simplessentails.eventHandlers;

import com.neutralplasma.simplessentails.SimpleEssentials;
import com.neutralplasma.simplessentails.flyManager.FlyTimer;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class LeaveEvent implements Listener {
    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event){
        FlyTimer.getInstance().savePlayer(event.getPlayer());
    }
}
