package com.neutralplasma.simplessentails.eventHandlers;

import com.neutralplasma.simplessentails.SimpleEssentials;
import com.neutralplasma.simplessentails.sellSticks.StickHandler;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.DoubleChestInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.PlayerInventory;


public class ClickEvent implements Listener {
    private SimpleEssentials plugin = SimpleEssentials.getInstance();
    private StickHandler stickHandler;

    public ClickEvent(StickHandler stickHandler){
        this.stickHandler = stickHandler;
    }
    @EventHandler(priority = EventPriority.HIGHEST)
    public void clickEvent(PlayerInteractEvent event){
        if(event.isCancelled()){
            return;
        }
        Player player = event.getPlayer();
        if(plugin.isShopLoaded()) {
            if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                PlayerInventory inventory = player.getInventory();
                Block block = event.getClickedBlock();
                BlockState blockState = block.getState();
                //Player r-clicked chest.
                if (blockState instanceof Chest) {
                    Material material = Material.getMaterial(plugin.getConfig().getString("sellStickFormat.material"));
                    if(inventory.getItemInMainHand().getType() == material){
                        Chest chest = (Chest) blockState;
                        Inventory chestInventory = chest.getInventory();
                        //Clicked double chest
                        if (chestInventory instanceof DoubleChestInventory) {
                            stickHandler.doWork(chestInventory, player, event);
                        } else { // Player clicked 1 block chest.
                            stickHandler.doWork(chestInventory, player, event);
                        }
                    }

                }
            }
        }
    }

}
