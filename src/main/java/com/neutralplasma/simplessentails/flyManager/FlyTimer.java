package com.neutralplasma.simplessentails.flyManager;

import com.neutralplasma.simplessentails.SimpleEssentials;
import com.neutralplasma.simplessentails.mainManagers.PlayerManager;
import com.neutralplasma.simplessentails.mainManagers.TextFormater;
import com.neutralplasma.simplessentails.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.UUID;

public class FlyTimer {
    private HashMap<UUID, Long> players = new HashMap<>();
    private PlayerUtil playerUtil;
    private static FlyTimer INSTANCE = null;

    public FlyTimer(PlayerUtil playerUtil){
        this.playerUtil = playerUtil;
        INSTANCE = this;
    }

    public void startFlyUpdater(){
        new BukkitRunnable(){
            @Override
            public void run() {
                for(UUID playerUuid : players.keySet()){
                    Player player = Bukkit.getPlayer(playerUuid);
                    if(!player.hasPermission("simpleessentials.fly.ignoretimer")){
                        if(player.getAllowFlight()){
                            long flyTime = players.get(playerUuid);
                            if(flyTime > 0){
                                flyTime --;
                                if(flyTime < 11){
                                    String time = String.valueOf(flyTime);
                                    String message = SimpleEssentials.getMessagesManager().getMessages().getString("flyTimerCountDown");
                                    message = message.replace("{0}", time);
                                    player.sendMessage(TextFormater.ColorFormat(message));
                                }
                            }
                            if(flyTime <= 0){
                                player.setAllowFlight(false);
                            }
                            players.put(playerUuid, flyTime);
                        }
                    }
                }
            }
        }.runTaskTimer(SimpleEssentials.getInstance(),0L, 20L);
    }

    public void addPlayer(UUID uuid){
        long timer = PlayerManager.getInstance().getFile(uuid.toString()).getLong("flyTimer");
        players.put(uuid, timer);
    }

    public void savePlayer(Player player){
        UUID uuid = player.getUniqueId();
        long timer = players.get(uuid);
        playerUtil.updatePlayerDataLong(player, "flyTimer", timer);
        players.remove(uuid);
    }
    public long getPlayerFly(Player player){
        UUID uuid = player.getUniqueId();
        long timer = players.get(uuid);
        return timer;
    }

    public static FlyTimer getInstance(){
        return INSTANCE;
    }

}
