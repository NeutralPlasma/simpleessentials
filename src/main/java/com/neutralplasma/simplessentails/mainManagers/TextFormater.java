package com.neutralplasma.simplessentails.mainManagers;

import com.neutralplasma.simplessentails.SimpleEssentials;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TextFormater {
    public static String ColorFormat(String text){
       String formated = ChatColor.translateAlternateColorCodes('&', text);
       return formated;
    }
    public static String timerFormat(Long time){
        String fullyFormatted;
        DateFormat dateFormat = new SimpleDateFormat("DDD-HH-mm-ss-SSS");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String formattedDate = dateFormat.format(new Date(time*1000L));
        String parts[] = formattedDate.split("-");
        Long days = Long.parseLong(parts[0]);
        days--;
        fullyFormatted = days + "d " + parts[1] + "h " + parts[2] + "m " + parts[3] + "s";
        if(days < 1){
            fullyFormatted = parts[1] + "h " + parts[2] + "m " + parts[3] + "s";
            if(parts[1].equals("00")){
                fullyFormatted = parts[2] + "m " + parts[3] + "s";
                if(parts[2].equals("00")){
                    fullyFormatted = parts[3] + "s";
                }
            }
        }
        return fullyFormatted;
    }

    public static void noPermissionMessage(CommandSender sender, String commandPermission){
        String unformated = SimpleEssentials.getMessagesManager().getMessages().getString("noPermission");
        unformated = unformated.replace("{0}", commandPermission);
        sender.sendMessage(ColorFormat(unformated));
    }
}
