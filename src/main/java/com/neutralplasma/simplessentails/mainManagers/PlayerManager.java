package com.neutralplasma.simplessentails.mainManagers;

import com.neutralplasma.simplessentails.SimpleEssentials;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;

public class PlayerManager {

    private Plugin pl = SimpleEssentials.getInstance();
    private static PlayerManager INSTANCE = null;
    public FileConfiguration fileConfiguration;
    public File file = new File(pl.getDataFolder() , "null");

    public PlayerManager(){
        INSTANCE = this;
    }

    public static PlayerManager getInstance(){
        return INSTANCE;
    }

    public void createFile(String fileName){
        //create userdata
        File folderfile = new File(pl.getDataFolder(), "userdata");
        //Create plugin folder
        if(!pl.getDataFolder().exists()){
            pl.getDataFolder().mkdir();
        }
        if(!folderfile.exists()){
            folderfile.mkdir();
        }

        //File
        file = new File(pl.getDataFolder() + "/userdata", fileName + ".yml");

        if(!file.exists()){
            try{
                file.createNewFile();
                fileConfiguration = YamlConfiguration.loadConfiguration(file);
                pl.getLogger().info("Creating new file at userdata folder: " + fileName);
            }catch (IOException error){
                error.printStackTrace();
                SimpleEssentials.getInstance().getLogger().severe("Something went wrong while creating file. Error: " + error.getMessage());
            }
        }
        fileConfiguration = YamlConfiguration.loadConfiguration(file);
    }

    public FileConfiguration getFile(String fileName){
        File nfile = new File(pl.getDataFolder() + "/userdata", fileName + ".yml");
        if(!nfile.exists()){
            createFile(fileName);
        }

        if(file.equals(nfile)){
            return fileConfiguration;
        }else{
            file = new File(pl.getDataFolder() + "/userdata", fileName + ".yml");
            fileConfiguration = YamlConfiguration.loadConfiguration(file);
            return fileConfiguration;
        }
    }


    public void saveFile(String fileName){
        file = new File(pl.getDataFolder() + "/userdata", fileName + ".yml");
        try{
            fileConfiguration.save(file);
            fileConfiguration = YamlConfiguration.loadConfiguration(file);
        }catch (IOException error){
            SimpleEssentials.getInstance().getLogger().severe("Something went wrong. Error: " + error.getMessage());
        }
    }

    public void reloadFile(String fileName){
        file = new File(pl.getDataFolder() + "/userdata", fileName + ".yml");
        fileConfiguration = YamlConfiguration.loadConfiguration(file);
    }
}
