package com.neutralplasma.simplessentails.mainManagers;

import com.neutralplasma.simplessentails.SimpleEssentials;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;

public class MessagesManager {
    private Plugin plugin = SimpleEssentials.getInstance();
    public FileConfiguration messagesconfiguration;
    public File messagesFile;
    public FileConfiguration dataFileonfiguration;
    public File dataFile;


    public void setup(){

        //creates plugin folder
        if(!plugin.getDataFolder().exists()){
            plugin.getDataFolder().mkdir();
        }
        //---------------------

        messagesFile = new File(plugin.getDataFolder(), "messages.yml");
        dataFile = new File(plugin.getDataFolder(), "data.yml");
        if(!messagesFile.exists()){
            try{
                messagesFile.createNewFile();
                messagesconfiguration = YamlConfiguration.loadConfiguration(messagesFile);
                plugin.saveResource("messages.yml", true);
                Bukkit.getConsoleSender().sendMessage( TextFormater.ColorFormat("&aSuccessfully created messages.yml file!"));


            }catch (IOException e){
                Bukkit.getConsoleSender().sendMessage( TextFormater.ColorFormat("&cFailed to create messages.yml file, Error: &f" + e.getMessage()));

            }

        }
        if(!dataFile.exists()){
            try {
                dataFile.createNewFile();
                dataFileonfiguration = YamlConfiguration.loadConfiguration(dataFile);
            }catch (IOException error){
                Bukkit.getConsoleSender().sendMessage( TextFormater.ColorFormat("&cFailed to create data.yml file, Error: &f" + error.getMessage()));
            }
        }
        messagesconfiguration = YamlConfiguration.loadConfiguration(messagesFile);
        dataFileonfiguration = YamlConfiguration.loadConfiguration(dataFile);
    }

    public FileConfiguration getMessages() {
        return messagesconfiguration;
    }
    public FileConfiguration getData(){
        return dataFileonfiguration;
    }


    public void saveMessages(){
        try{
            messagesconfiguration.save(messagesFile);
            Bukkit.getConsoleSender().sendMessage(TextFormater.ColorFormat("&aSuccessfully saved messages.yml file."));
        }catch(IOException e){
            Bukkit.getConsoleSender().sendMessage(TextFormater.ColorFormat("&cFailed to save messages.yml file, Error: &f" + e.getMessage()));
        }
    }
    public void saveData(){
        try{
            dataFileonfiguration.save(dataFile);
        }catch (IOException error){
            Bukkit.getConsoleSender().sendMessage(TextFormater.ColorFormat("&cFailed to save data.yml file, Error: &f" + error.getMessage()));
        }
    }

    public void reloadMessages() {
        messagesconfiguration = YamlConfiguration.loadConfiguration(messagesFile);
        Bukkit.getConsoleSender().sendMessage(TextFormater.ColorFormat("&aReloaded messages.yml file."));
    }
    public void reloadData(){
        dataFileonfiguration = YamlConfiguration.loadConfiguration(dataFile);
        Bukkit.getConsoleSender().sendMessage(TextFormater.ColorFormat("&aReloaded data.yml file."));
    }


}
