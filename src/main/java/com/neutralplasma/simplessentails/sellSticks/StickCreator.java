package com.neutralplasma.simplessentails.sellSticks;

import com.neutralplasma.simplessentails.SimpleEssentials;
import com.neutralplasma.simplessentails.mainManagers.TextFormater;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.tags.CustomItemTagContainer;
import org.bukkit.inventory.meta.tags.ItemTagType;

import java.util.ArrayList;
import java.util.List;

public class StickCreator {
    SimpleEssentials plugin = SimpleEssentials.getInstance();

    public ItemStack createStick(Long uses){
        String nameFormat = plugin.getConfig().getString("sellStickFormat.name");
        List list = plugin.getConfig().getList("sellStickFormat.lore");
        List<String> lore = new ArrayList<>();
        Material material = Material.getMaterial(plugin.getConfig().getString("sellStickFormat.material"));
        ItemStack item = new ItemStack(material);
        NamespacedKey key = new NamespacedKey(plugin, "uses");
        ItemMeta itemMeta = item.getItemMeta();

        itemMeta.getCustomTagContainer().setCustomTag(key, ItemTagType.LONG, uses);
        itemMeta.setDisplayName(TextFormater.ColorFormat(nameFormat));
        for(Object object : list.toArray()){
            String loreline = object.toString();
            if(loreline.contains("{0}")){
                loreline = loreline.replace("{0}", String.valueOf(uses));
            }
            lore.add(TextFormater.ColorFormat(loreline));
        }
        itemMeta.setLore(lore);
        item.setItemMeta(itemMeta);

        return item;
    }
    public Long getUses(ItemStack item){
        NamespacedKey key = new NamespacedKey(plugin, "uses");
        ItemMeta itemMeta = item.getItemMeta();
        CustomItemTagContainer tagContainer = itemMeta.getCustomTagContainer();
        if(tagContainer.hasCustomTag(key , ItemTagType.LONG)) {
            long foundValue = tagContainer.getCustomTag(key, ItemTagType.LONG);
            return foundValue;
        }
        if(SimpleEssentials.getDebug()){
            Bukkit.getConsoleSender().sendMessage("Hmm " + tagContainer.getAdapterContext());
        }
        return -2L;
    }

    public void updateStick(Player player, ItemStack itemStack, long newValue){
        PlayerInventory playerInventory = player.getInventory();
        playerInventory.setItemInMainHand(createStick(newValue));
    }

    public ItemStack setCustomTag(ItemStack item){
        String serverVersion = Bukkit.getVersion();
        return item;
    }
}
