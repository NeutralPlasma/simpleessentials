package com.neutralplasma.simplessentails.sellSticks;
import com.neutralplasma.simplessentails.SimpleEssentials;
import com.neutralplasma.simplessentails.mainManagers.TextFormater;
import net.brcdev.shopgui.ShopGuiPlusApi;
import net.brcdev.shopgui.api.exception.PlayerDataNotLoadedException;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class StickHandler {
    SimpleEssentials plugin = SimpleEssentials.getInstance();
    private StickCreator stickCreator;

    public  StickHandler(StickCreator stickCreator){
        this.stickCreator = stickCreator;
    }

    public void doWork(Inventory inventory, Player player, PlayerInteractEvent event){
        PlayerInventory pinventroy = player.getInventory();
        ItemStack pitem = pinventroy.getItemInMainHand();
        long uses = stickCreator.getUses(pitem);
        if(uses > 0) {
            if (player.hasPermission("simpleessentials.sellstick.use")) {
                event.setCancelled(true);
                Economy econ = plugin.getEconomy();
                double totalyMoneyAdded = 0;
                for (ItemStack item : inventory.getContents()) {
                    try {
                        double moneyToAdd = ShopGuiPlusApi.getItemStackPriceSell(player, item);
                        if (moneyToAdd < 1) {
                            moneyToAdd = 0;
                        } else {
                            inventory.remove(item);
                        }
                        totalyMoneyAdded += moneyToAdd;
                        EconomyResponse er = econ.depositPlayer(player, moneyToAdd);
                    } catch (PlayerDataNotLoadedException error) {
                    }

                }
                if(totalyMoneyAdded > 0){
                    String message = plugin.getMessagesManager().getMessages().getString("usedSellStick");
                    message = message.replace("{0}", String.valueOf(totalyMoneyAdded));
                    player.sendMessage(TextFormater.ColorFormat(message));
                    uses--;
                    if(uses < 1){
                        uses = 0;
                    }
                    stickCreator.updateStick(player, pitem, uses);
                }
            }
        }else{
            if(!(uses == -2L)){
                pinventroy.remove(pitem);
            }
        }
    }
}
