package com.neutralplasma.simplessentails;

import com.neutralplasma.simplessentails.commands.SEssCommand;
import com.neutralplasma.simplessentails.commands.tabCompleters.MainTabComplete;
import com.neutralplasma.simplessentails.eventHandlers.ClickEvent;
import com.neutralplasma.simplessentails.eventHandlers.JoinEvent;
import com.neutralplasma.simplessentails.eventHandlers.LeaveEvent;
import com.neutralplasma.simplessentails.mainManagers.MessagesManager;
import com.neutralplasma.simplessentails.mainManagers.PlayerManager;
import com.neutralplasma.simplessentails.flyManager.FlyTimer;
import com.neutralplasma.simplessentails.sellSticks.StickCreator;
import com.neutralplasma.simplessentails.sellSticks.StickHandler;
import com.neutralplasma.simplessentails.utils.PlayerUtil;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;


public class SimpleEssentials extends JavaPlugin {
    private static SimpleEssentials INSTANCE = null;
    private static FlyTimer flyTimer;
    private static MessagesManager messagesManager;
    private static MainTabComplete mainTabComplete;
    private PlayerManager playerManager;
    private PlayerUtil playerUtil;
    private static StickCreator stickCreator;
    private StickHandler stickHandler;
    private Integer test;
    private static boolean debug;


    public boolean shopEnabled;
    private static Economy econ = null;

    @Override
    public void onEnable() {
        INSTANCE = this;
        playerManager = new PlayerManager();
        playerUtil = new PlayerUtil();
        flyTimer = new FlyTimer(playerUtil);
        messagesManager = new MessagesManager();
        mainTabComplete = new MainTabComplete();

        setupCommands();
        setupPlayers();
        setupConfig();
        flyTimer.startFlyUpdater();
        //setup messages
        messagesManager.setup();
        //Event Handlers
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new JoinEvent(playerUtil), this);
        pluginManager.registerEvents(new LeaveEvent(), this);
        if(Bukkit.getPluginManager().getPlugin("ShopGuiPlus") != null) {
            shopEnabled = true;
            stickCreator = new StickCreator();
            stickHandler = new StickHandler(stickCreator);
            pluginManager.registerEvents(new ClickEvent(stickHandler), this);
            Bukkit.getConsoleSender().sendMessage("Enabled sellSticks");
        }else{
            shopEnabled = false;
            stickCreator = null;
            stickHandler = null;
            Bukkit.getConsoleSender().sendMessage("Disabled sellSticks");
        }
        if (!setupEconomy() ) {
            getLogger().severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
        }
        debug = this.getConfig().getBoolean("debug");
    }

    @Override
    public void onDisable() {
        for(Player player : Bukkit.getOnlinePlayers()){
            flyTimer.savePlayer(player);
        }
    }
    //All instances
    public static SimpleEssentials getInstance() {
        return INSTANCE;
    }
    public static MessagesManager getMessagesManager(){
        return messagesManager;
    }
    public static MainTabComplete getMainTabComplete(){
        return mainTabComplete;
    }
    public static StickCreator getStickCreator(){
        return stickCreator;
    }
    public static boolean getDebug(){
        return debug;
    }
    //Setup all commands
    public void setupCommands(){
        SEssCommand.registerCommands(this);
    }

    //Setup UserData
    public void setupPlayers(){
        new BukkitRunnable(){
            @Override
            public void run() {
                for (OfflinePlayer oplayer : Bukkit.getOfflinePlayers()) {
                    playerUtil.setupOfflinePlayer(oplayer);
                }
            }
        }.runTaskAsynchronously(this);
    }

    public void setupConfig(){
        this.saveDefaultConfig();
    }

    public boolean isShopLoaded(){
        return shopEnabled;
    }
    //setup ECONOMY
    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
    //get economy
    public Economy getEconomy() {
        return econ;
    }
}
