package com.neutralplasma.simplessentails.utils;

import com.neutralplasma.simplessentails.SimpleEssentials;
import com.neutralplasma.simplessentails.mainManagers.PlayerManager;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.io.File;


public class PlayerUtil {
    private PlayerManager pm = PlayerManager.getInstance();
    private static PlayerUtil INSTANCE = null;

    public PlayerUtil(){
        INSTANCE = this;
    }
    public void setupPlayer(Player player){
        String fileName = player.getUniqueId().toString();
        if(!playerExists(player)){
            pm.createFile(fileName);
            pm.getFile(fileName).set("lastNameUsed", player.getName());
            pm.getFile(fileName).set("ipAddress", player.getAddress());
            pm.getFile(fileName).set("flyTimer", 0L);
            pm.saveFile(fileName);
        }
    }
    public void setupOfflinePlayer(OfflinePlayer player){
        String fileName = player.getUniqueId().toString();
        if(!offlinePlayerExists(player)){
            pm.createFile(fileName);
            pm.getFile(fileName).set("lastNameUsed", player.getName());
            pm.getFile(fileName).set("ipAddress", 0L);
            pm.getFile(fileName).set("flyTimer", 0L);
            pm.saveFile(fileName);
        }
    }
    public void updatePlayerDataString(Player player, String toUpdate, String updateParameters){
        String fileName = player.getUniqueId().toString();
        if(!playerExists(player)){
            setupPlayer(player);
        }
        pm.getFile(fileName).set(toUpdate, updateParameters);
        pm.saveFile(fileName);
    }
    public void updatePlayerDataObject(Player player, String toUpdate, Object updateParameters){
        String fileName = player.getUniqueId().toString();
        if(!playerExists(player)){
            setupPlayer(player);
        }
        pm.getFile(fileName).set(toUpdate, updateParameters);
        pm.saveFile(fileName);
    }
    public void updatePlayerDataLong(Player player, String toUpdate, Long updateParameters){
        String fileName = player.getUniqueId().toString();
        if(!playerExists(player)){
            setupPlayer(player);
        }
        pm.getFile(fileName).set(toUpdate, updateParameters);
        pm.saveFile(fileName);
    }
    public boolean playerExists(Player player){
        String fileName = player.getUniqueId().toString();
        File nfile = new File(SimpleEssentials.getInstance().getDataFolder() + "/userdata", fileName + ".yml");
        if(!nfile.exists()){
            return false;
        }else{
            return true;
        }
    }
    public boolean offlinePlayerExists(OfflinePlayer player){
        String fileName = player.getUniqueId().toString();
        File nfile = new File(SimpleEssentials.getInstance().getDataFolder() + "/userdata", fileName + ".yml");
        if(!nfile.exists()){
            return false;
        }else{
            return true;
        }
    }

    public static PlayerUtil getInstance(){
        return INSTANCE;
    }
}
