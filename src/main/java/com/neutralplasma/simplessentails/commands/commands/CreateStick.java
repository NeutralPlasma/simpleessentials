package com.neutralplasma.simplessentails.commands.commands;

import com.neutralplasma.simplessentails.SimpleEssentials;
import com.neutralplasma.simplessentails.commands.SEssCommand;
import com.neutralplasma.simplessentails.mainManagers.TextFormater;
import com.neutralplasma.simplessentails.sellSticks.StickCreator;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class CreateStick extends SEssCommand {
    private StickCreator stickCreator = SimpleEssentials.getStickCreator();
    public CreateStick(){
        super("createstick", "simpleessentials.createstick", true);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        Player player = null;
        Long uses;
        if(args.length > 1) {
            player = Bukkit.getPlayer(args[0]);
        }
        if(args.length > 1 && player != null){
            try {
                uses = Long.parseLong(args[1]);
            }catch (Exception error){
                String string = SimpleEssentials.getMessagesManager().getMessages().getString("unknownArgs");
                sender.sendMessage(TextFormater.ColorFormat(string));
                return;
            }
            ItemStack item = stickCreator.createStick(uses);
            PlayerInventory inventory = player.getInventory();
            inventory.addItem(item);
        }else{
            if(args.length < 2){
                sender.sendMessage(TextFormater.ColorFormat(SimpleEssentials.getMessagesManager().getMessages().getString("unknownPlayer")));
            }
        }
    }
}
