package com.neutralplasma.simplessentails.commands.commands;

import com.neutralplasma.simplessentails.SimpleEssentials;
import com.neutralplasma.simplessentails.commands.SEssCommand;
import com.neutralplasma.simplessentails.mainManagers.TextFormater;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Map;

public class SetSpawnCommand extends SEssCommand {
    public SetSpawnCommand(){
        super("setsspawn", "simpleessentials.setspawn", false);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        Player player = (Player) sender;
        Location location = player.getLocation();
        SimpleEssentials.getMessagesManager().getData().set("spawnLocation.x", location.getX());
        SimpleEssentials.getMessagesManager().getData().set("spawnLocation.y", location.getY());
        SimpleEssentials.getMessagesManager().getData().set("spawnLocation.z", location.getZ());
        SimpleEssentials.getMessagesManager().getData().set("spawnLocation.world", location.getWorld().getName());
        SimpleEssentials.getMessagesManager().getData().set("spawnLocation.yaw", location.getYaw());
        SimpleEssentials.getMessagesManager().getData().set("spawnLocation.pitch", location.getPitch());
        SimpleEssentials.getMessagesManager().saveData();
        String message = SimpleEssentials.getMessagesManager().getMessages().getString("setSpawnCommand");
        message = message.replace("{0}", String.valueOf(location.getX()));
        message = message.replace("{1}", String.valueOf(location.getY()));
        message = message.replace("{2}", String.valueOf(location.getZ()));
        message = message.replace("{3}", location.getWorld().getName());
        player.sendMessage(TextFormater.ColorFormat(message));
    }
}
