package com.neutralplasma.simplessentails.commands.commands;

import com.neutralplasma.simplessentails.SimpleEssentials;
import com.neutralplasma.simplessentails.commands.SEssCommand;
import com.neutralplasma.simplessentails.flyManager.FlyTimer;
import com.neutralplasma.simplessentails.mainManagers.PlayerManager;
import com.neutralplasma.simplessentails.mainManagers.TextFormater;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FlyCommand extends SEssCommand {

    public FlyCommand() {
        super("fly", "simpleessentials.fly", true);
        Bukkit.getConsoleSender().sendMessage("Enabled fly command.");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (!(sender instanceof Player)) {
            Player target = null;
            if (args.length > 0) {
                checkSubCommands(args, sender);
            }else{
                String unformated = SimpleEssentials.getMessagesManager().getMessages().getString("flySpecifyPlayer");
                sender.sendMessage(TextFormater.ColorFormat(unformated));
            }
        } else {
            Player target = null;
            if (args.length > 0) {
                checkSubCommands(args, sender);
            }else{
                if(sender.hasPermission("simpleessentials.fly")) {
                    if(FlyTimer.getInstance().getPlayerFly((Player) sender) > 0) {
                        target = (Player) sender;
                        updateFly(target);
                        String unformated = SimpleEssentials.getMessagesManager().getMessages().getString("updatedSelfFly");
                        unformated = unformated.replace("{0}", getFlyState(target));
                        target.sendMessage(TextFormater.ColorFormat(unformated));
                    }else{
                        TextFormater.noPermissionMessage(sender, "simpleessentials.fly");
                    }
                }
                else{
                    TextFormater.noPermissionMessage(sender, "simpleessentials.fly.timer");
                }
            }
        }
    }
    public void checkSubCommands(String[] args, CommandSender sender) {
        //targeted player.
        Player target = Bukkit.getPlayer(args[0]);
        if (target == null) {
            //Hmm need to to permissions check.
            TextFormater.noPermissionMessage(sender, "simpleessentials.fly.other");
            return;
        }
        //Not finished.
        if (args.length > 2) {
            //Fly timer add time
            if (args[1].equals("add")) {
                if (sender.hasPermission("simpleessentials.fly.timer")) {
                    Long timer = Long.parseLong(args[2]);
                    Long oldtimer = FlyTimer.getInstance().getPlayerFly(target);
                    PlayerManager.getInstance().getFile(target.getUniqueId().toString()).set("flyTimer", timer + oldtimer);
                    PlayerManager.getInstance().saveFile(target.getUniqueId().toString());
                    FlyTimer.getInstance().addPlayer(target.getUniqueId());
                    String unformated = SimpleEssentials.getMessagesManager().getMessages().getString("flyTimerUpdate");
                    unformated = unformated.replace("{0}", target.getName());
                    unformated = unformated.replace("{1}", TextFormater.timerFormat(timer + oldtimer));
                    unformated = unformated.replace("{2}", TextFormater.timerFormat(oldtimer));
                    sender.sendMessage(TextFormater.ColorFormat(unformated));
                } else {
                    TextFormater.noPermissionMessage(sender, "simpleessentials.fly.timer");
                }
            }
            //fly timer remove time
            if (args[1].equals("remove")) {
                if (sender.hasPermission("simpleessentials.fly.timer")) {
                    Long timer = Long.parseLong(args[2]);
                    Long oldtimer = FlyTimer.getInstance().getPlayerFly(target);
                    PlayerManager.getInstance().getFile(target.getUniqueId().toString()).set("flyTimer", oldtimer - timer);
                    PlayerManager.getInstance().saveFile(target.getUniqueId().toString());
                    FlyTimer.getInstance().addPlayer(target.getUniqueId());
                    String unformated = SimpleEssentials.getMessagesManager().getMessages().getString("flyTimerUpdate");
                    unformated = unformated.replace("{0}", target.getName());
                    unformated = unformated.replace("{1}", TextFormater.timerFormat(oldtimer - timer));
                    unformated = unformated.replace("{2}", TextFormater.timerFormat(oldtimer));
                    sender.sendMessage(TextFormater.ColorFormat(unformated));
                } else {
                    TextFormater.noPermissionMessage(sender, "simpleessentials.fly.timer");
                }
            }

        } else {
            if (sender instanceof Player) {
                Player psender = (Player) sender;
                //Check if player has permission to update others fly mode.
                if (psender.hasPermission("simpleessentials.fly.other") && !(psender == target)) {
                    updateFly(target);
                    String unformated = SimpleEssentials.getMessagesManager().getMessages().getString("updatedOtherFly");
                    unformated = unformated.replace("{0}", getFlyState(target));
                    unformated = unformated.replace("{1}", target.getName());
                    String unformated2 = SimpleEssentials.getMessagesManager().getMessages().getString("notifyFlyUpdateTarget");
                    unformated2 = unformated2.replace("{0}", getFlyState(target));

                    target.sendMessage(TextFormater.ColorFormat(unformated2));
                    psender.sendMessage(TextFormater.ColorFormat(unformated));
                } else {
                    // If he sets target as himself.
                    if (psender == target) {
                        if (FlyTimer.getInstance().getPlayerFly(target) > 0) {
                            updateFly(target);
                            String unformated = SimpleEssentials.getMessagesManager().getMessages().getString("updatedSelfFly");
                            unformated = unformated.replace("{0}", getFlyState(target));

                            psender.sendMessage(TextFormater.ColorFormat(unformated));
                        } else {
                            TextFormater.noPermissionMessage(sender, "simpleessentials.fly");
                        }
                    } else {
                        TextFormater.noPermissionMessage(sender, "simpleessentials.fly.other");
                    }
                }

            } else {
                //If sender is console.
                updateFly(target);
                String unformated = SimpleEssentials.getMessagesManager().getMessages().getString("updatedOtherFly");
                unformated = unformated.replace("{0}", getFlyState(target));
                unformated = unformated.replace("{1}", target.getName());
                sender.sendMessage(TextFormater.ColorFormat(unformated));
            }

        }
    }


    //updating users fly mode.
    public void updateFly (Player target){
        if (target == null) {
            return;
        }
        if (target.getAllowFlight()) {
            target.setAllowFlight(false);
        } else {
            target.setAllowFlight(true);
        }
    }

    //check if user has fly enabled or no.
    public String getFlyState(Player target){
        if (target == null) {
            return null;
        }
        if (target.getAllowFlight()) {
            return "enabled";
        } else{
            return "disabled";
        }
    }

}
