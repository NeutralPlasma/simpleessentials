package com.neutralplasma.simplessentails.commands.commands;

import com.neutralplasma.simplessentails.SimpleEssentials;
import com.neutralplasma.simplessentails.commands.SEssCommand;
import com.neutralplasma.simplessentails.mainManagers.MessagesManager;
import com.neutralplasma.simplessentails.mainManagers.TextFormater;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpawnCommand extends SEssCommand {

    public SpawnCommand() {
        super("sspawn", "simpleessentials.spawn", false);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        MessagesManager data = SimpleEssentials.getMessagesManager();
        if(args.length >= 1){
            //If player entered args after /spawn, Check if he has permission to spawn other people.
            if(sender.hasPermission("simpleessentials.spawn.other")){
                Player target;
                target = Bukkit.getPlayerExact(args[0]);
                if (target == null) {
                    sender.sendMessage(TextFormater.ColorFormat(data.getMessages().getString("unknownPlayer")));
                }else {

                    String messageSender = data.getMessages().getString("teleportOther");
                    messageSender = messageSender.replace("{0}", target.getName());
                    sender.sendMessage(TextFormater.ColorFormat(messageSender));
                    teleport(target);
                }
            }else
                TextFormater.noPermissionMessage(sender, "simpleessentials.spawn.other");
        }else{
            teleport((Player) sender);
        }

    }

    public void teleport(Player player){
        MessagesManager data = SimpleEssentials.getMessagesManager();
        String message = data.getMessages().getString("teleport");
        player.sendMessage(TextFormater.ColorFormat(message));
        if (!(data.getData().getString("spawnLocation") == null)) {
            Location location = null;
            location = Location.deserialize(data.getData().getConfigurationSection("spawnLocation").getValues(true));
            player.teleport(location);
            player.sendMessage(TextFormater.ColorFormat(data.getMessages().getString("successfullyTeleported")));
        }else{
            Location location = Bukkit.getServer().getWorlds().get(0).getSpawnLocation();
            player.teleport(location);
            player.sendMessage(TextFormater.ColorFormat(data.getMessages().getString("successfullyTeleported")));
        }
    }


}
