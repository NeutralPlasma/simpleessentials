package com.neutralplasma.simplessentails.commands.commands;

import com.neutralplasma.simplessentails.commands.SEssCommand;
import org.bukkit.command.CommandSender;

public class TestCommand extends SEssCommand {

    public TestCommand(){
        super("testcommand", "testcommand.use", true);
    }

    public final void execute(final CommandSender sender, final String[] arg){
        sender.sendMessage("Test");
    }
}
