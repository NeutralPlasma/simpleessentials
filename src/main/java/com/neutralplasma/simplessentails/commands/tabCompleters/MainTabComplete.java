package com.neutralplasma.simplessentails.commands.tabCompleters;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class MainTabComplete implements TabCompleter {
    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        List<String> players = new ArrayList<>();
        //get online players
        for(Player player : Bukkit.getOnlinePlayers()){
            players.add(player.getName());
        }
        List<String> listedPlayers = new ArrayList<>();
        List<String> empty = new ArrayList<>();
        //fly Command tab Complete.
        if(command.getName().equals("fly")){
            //List of subcommands
            List<String> subCommands = new ArrayList<>();
            subCommands.add("add");
            subCommands.add("remove");
            //if player doesnt enter any player and he wants auto tab complete return online players if he has permission for it.
            if(strings.length < 2){
                if(commandSender.hasPermission("simpleessentials.fly.other")){
                    for(String player : players){
                        String lplayer;
                        lplayer = player.toLowerCase();
                        if(player.contains(strings[0]) || lplayer.contains(strings[0])){
                            listedPlayers.add(player);
                        }
                    }
                    return listedPlayers;
                }else{
                    return empty;
                }
            }else{
                //if player enters player.
                if(commandSender.hasPermission("simpleessentials.fly.timer") && strings.length == 2){
                    return subCommands;
                }else{
                    return empty;
                }
            }
        }
        if(command.getName().equals("spawn")){
            if(strings.length >= 2){
                if(commandSender.hasPermission("simpleessentials.spawn.other")){
                    for(String player : players){
                        String lplayer;
                        lplayer = player.toLowerCase();
                        if(player.contains(strings[0]) || lplayer.contains(strings[0])){
                            listedPlayers.add(player);
                        }
                    }
                    return listedPlayers;
                }else{
                    return empty;
                }
            }
        }

        return null;
    }
}
