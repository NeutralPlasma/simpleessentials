package com.neutralplasma.simplessentails.commands;

import com.neutralplasma.simplessentails.SimpleEssentials;
import com.neutralplasma.simplessentails.commands.commands.*;
import com.neutralplasma.simplessentails.mainManagers.TextFormater;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class SEssCommand implements CommandExecutor {

    private final String commandName;
    private final String permission;
    private final boolean canConsoleUse;
    public static SimpleEssentials plugin;

    public SEssCommand(String commandName, String permission, boolean canConsoleUse){
        this.commandName = commandName;
        this.permission = permission;
        this.canConsoleUse = canConsoleUse;
        SimpleEssentials.getInstance().getCommand(commandName).setExecutor(this);
        SimpleEssentials.getInstance().getCommand(commandName).setTabCompleter(SimpleEssentials.getMainTabComplete());
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String str, String[] args) {
        if(!cmd.getLabel().equalsIgnoreCase(commandName))
            return true;
        if(!sender.hasPermission(permission)){
            TextFormater.noPermissionMessage(sender, permission);
            return true;
        }
        if(!canConsoleUse && !(sender instanceof Player)){
            sender.sendMessage("Only players may use this command sorry!");
            return true;
        }
        execute(sender, args);
        return true;
    }
    public final static void registerCommands(SimpleEssentials pl){
        plugin = pl;
        new TestCommand();
        new FlyCommand();
        new SpawnCommand();
        new SetSpawnCommand();
        new CreateStick();
    }

    public abstract void execute(CommandSender sender, String[] args);
}
